from django.db import models


class RedirectLinks(models.Model):
    link = models.CharField(unique=True, max_length=6)
    url = models.URLField(unique=True, max_length=65535)
    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.link
