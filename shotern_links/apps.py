from django.apps import AppConfig


class ShoternLinksConfig(AppConfig):
    name = 'shotern_links'
