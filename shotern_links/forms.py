from django import forms

class AddUrlForm(forms.Form):
    url = forms.URLField()
