from django.core.management.base import BaseCommand, CommandError
from shotern_links.models import RedirectLinks
from datetime import timedelta
from django.utils import timezone


class Command(BaseCommand):
    help = 'Delete objects older than 10 days'

    def handle(self, *args, **options):
        try:
            print('timezone.now() =', timezone.now())
            print('h_now =', timezone.now() - timedelta(hours=1))  # TODO
            links = RedirectLinks.objects.filter(created_time__lte=timezone.now()-timedelta(hours=1))
            links.delete()
            self.stdout.write('Deleted objects older than 1 hour')
        except RedirectLinks.DoesNotExist:
            raise CommandError('uups')
