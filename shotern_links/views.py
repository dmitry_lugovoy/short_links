from django.views.generic.base import TemplateView
from django.views.generic import RedirectView
from shotern_links.service import Links
from .forms import AddUrlForm


class HomePageView(TemplateView):

    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['links_list'] = Links.get_links_list()
        return context


class AddShoternLink(RedirectView):
    """Add url to db"""
    permanent = True
    url = '/'

    def get_redirect_url(self, *args, **kwargs):
        form = AddUrlForm(self.request.POST)
        if form.is_valid():
            url = form.cleaned_data['url']
            print('url =', url)
            links = Links()
            links.add_url(url)
            return super().get_redirect_url(*args, **kwargs)


class GetShoternLink(RedirectView):
    """Redirect to url stored in db"""
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        link = kwargs['link']
        if link:
            links = Links()
            self.url = links.get_url(link)
        return super().get_redirect_url(*args, **kwargs)
