from django.urls import path
from .views import GetShoternLink, AddShoternLink, HomePageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('add', AddShoternLink.as_view(), name='add-link'),
    path('<str:link>', GetShoternLink.as_view(), name='get-link'),
]
