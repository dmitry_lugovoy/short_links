from django.conf import settings
from hashlib import blake2b
from shotern_links.models import RedirectLinks
from django.shortcuts import get_object_or_404
from django.utils.encoding import uri_to_iri


class Links:

    def get_url(self, link):
        """
        Get URL by short link
        :param link: str
        :return: url: str
        """
        obj = get_object_or_404(RedirectLinks, link=link)
        return uri_to_iri(obj.url)

    @classmethod
    def get_links_list(cls):
        return RedirectLinks.objects.all()

    def add_url(self, url):
        """
        add url
        :param url: str
        :return: short link: str
        """
        while True:
            link = self.string_to_hash(url)
            data = RedirectLinks.objects.filter(link=link)
            if not data:
                # если в базе нет такого ключа сохраняем запись
                link = RedirectLinks(link=link, url=url)
                link.save()
                return
            else:
                # если пришли сюда, значит коллизия, модифицируем url и опять пытаемся получить из него хэш
                # url += ' '
                print('data =', data)
                return

    @staticmethod
    def string_to_hash(str_url, hash_size=settings.SHORT_LINK_LEN):
        """
        Get hash from string
        :param str_url: str
        :param hash_size: int
        :return: string hash: str
        """
        h = blake2b(digest_size=int(hash_size/2))
        h.update(str_url.encode())
        return h.hexdigest()
