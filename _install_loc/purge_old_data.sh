#!/usr/bin/env bash

# load settings
source ./settings.sh

# activate conda env
source "$CONDA_PATH"/activate "$ENV_NAME"
python --version
cd ..
echo $(pwd)

python manage.py purge_old_data