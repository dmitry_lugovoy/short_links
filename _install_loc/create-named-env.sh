#!/usr/bin/env bash

# load settings
source ./settings.sh

# create conda env
#"$CONDA_PATH"/conda create --name="$ENV_NAME" python=3.7

# activate conda env
source "$CONDA_PATH"/activate "$ENV_NAME"
python --version
echo $(pwd)

pip install -r requirements.txt

cd ..
echo $(pwd)

python manage.py makemigrations
python manage.py makemigrations "$ENV_NAME"
python manage.py migrate